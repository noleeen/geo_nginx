module geo

go 1.19

require (
	github.com/ekomobile/dadata/v2 v2.11.0
	github.com/go-chi/chi v1.5.5
	github.com/go-chi/jwtauth v1.2.0
	github.com/lib/pq v1.10.9
	github.com/redis/go-redis/v9 v9.5.1
	golang.org/x/sync v0.6.0
)

require (
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/goccy/go-json v0.3.5 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/lestrrat-go/backoff/v2 v2.0.7 // indirect
	github.com/lestrrat-go/httpcc v1.0.0 // indirect
	github.com/lestrrat-go/iter v1.0.0 // indirect
	github.com/lestrrat-go/jwx v1.1.0 // indirect
	github.com/lestrrat-go/option v1.0.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
)
