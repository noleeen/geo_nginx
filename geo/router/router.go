package router

import (
	"geo/internal/modules/geo/controller"
	"geo/static"
	"github.com/go-chi/chi"
	"net/http"
)

func NewApiRouter(r *chi.Mux, controller geoController.GeoControllerer) http.Handler {

	r.Get("/swagger", static.SwaggerUI)
	r.Get("/static/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))).ServeHTTP(w, r)
	})

	r.Post("/api/address/search", controller.SearchAddressHandler)
	r.Post("/api/address/geocode", controller.GeocodeAddressHandler)

	r.Get("/api", HelloFromApi)

	return r
}

func HelloFromApi(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello from API"))
}
