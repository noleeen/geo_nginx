package config

import "os"

type DadataKeys struct {
	ApiKey    string
	SecretKey string
}

func GetDadataKeysConfig() DadataKeys {
	d := DadataKeys{}
	d.ApiKey = os.Getenv("API_KEY_VALUE")
	d.SecretKey = os.Getenv("SECRET_KEY_VALUE")

	return d
}
