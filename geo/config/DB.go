package config

type DB struct {
	Driver   string
	Host     string
	Port     string
	Username string
	Password string
	DbName   string
	SslMode  string
}

var defaultDbConfig = DB{
	Driver:   "postgres",
	Host:     "localhost",
	Port:     "5432",
	Username: "postgres",
	Password: "secret",
	DbName:   "users",
}

func GetDbConfig() DB {
	db := DB{}
	db.Driver = getEnv("DB_DRIVER", defaultDbConfig.Driver)
	db.Host = getEnv("DB_HOST", defaultDbConfig.Host)
	db.Port = getEnv("DB_PORT", defaultDbConfig.Port)
	db.Username = getEnv("DB_USER", defaultDbConfig.Username)
	db.Password = getEnv("DB_PASSWORD", defaultDbConfig.Password)
	db.DbName = getEnv("DB_NAME", defaultDbConfig.DbName)
	db.SslMode = getEnv("SSL_MODE", defaultDbConfig.SslMode)
	return db

}
