package run

import (
	"context"
	"database/sql"
	"fmt"
	"geo/config"
	geoController2 "geo/internal/modules/geo/controller"
	"geo/internal/modules/geo/service"
	"geo/pkg/responder"
	"geo/router"
	"geo/server/serverHttp"
	"github.com/go-chi/chi"
	_ "github.com/lib/pq"
	"github.com/redis/go-redis/v9"
	"golang.org/x/sync/errgroup"
	"log"
	"net/http"
	"os"
)

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() error
}

// App - структура приложения
type App struct {
	Conf config.Config
	//Rpc  server.Serverer
	Server serverHttp.Serverer
	Sig    chan os.Signal
}

func NewApp(conf *config.Config) *App {
	return &App{
		Conf: *conf,
		Sig:  make(chan os.Signal, 1),
	}
}

// Run - запуск приложения
func (a *App) Run() error {
	ctx, cancel := context.WithCancel(context.Background())
	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		log.Println("signal interrupt received", sigInt)
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.Server.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return err
	}

	return nil

}

func (a *App) Bootstrap(options ...interface{}) Runner {

	//postgresql
	dsn := a.Conf.GetDsnDB()
	fmt.Println("dsn:", dsn)
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		fmt.Println("error sql.Open")
		log.Fatal(err)
	}

	ctx := context.Background()

	//вот здесь запустим редис для быстрого кэширования запросов
	clientRedis := redis.NewClient(&redis.Options{
		Addr: "redis:6379",
		DB:   0,
	})
	// Проверка соединения с Redis
	pong, err := clientRedis.Ping(ctx).Result()
	if err != nil {
		fmt.Println("Ошибка соединения с Redis:", err)
		log.Println(err)
	}
	fmt.Println("Соединение с Redis успешно:", pong)

	geoService := service.NewGeoService(db, clientRedis, &a.Conf)
	resp := responder.NewResponder()

	geoController := geoController2.NewGeoController(geoService, resp)

	chiRouter := chi.NewRouter()

	r := router.NewApiRouter(chiRouter, geoController)

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.Conf.ServerHttp.Port),
		Handler: r,
	}
	// инициализация сервера
	a.Server = serverHttp.NewHttpServer(srv, a.Conf)

	return a
}
