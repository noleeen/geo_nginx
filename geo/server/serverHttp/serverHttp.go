package serverHttp

import (
	"context"
	"geo/config"
	"log"
	"net/http"
	"time"
)

type HttpServer struct {
	srv  *http.Server
	conf config.Config
}

func NewHttpServer(serv *http.Server, conf config.Config) Serverer {
	return &HttpServer{srv: serv, conf: conf}
}

func (s *HttpServer) Serve(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		log.Printf("server started on port %s", s.conf.ServerHttp.Port)
		if err = s.srv.ListenAndServe(); err != http.ErrServerClosed {
			log.Println("http listen and serve error", err)
			chErr <- err
		}
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
	}

	ctxShutdown, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	err = s.srv.Shutdown(ctxShutdown)

	return err
}
