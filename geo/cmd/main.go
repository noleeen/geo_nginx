package main

import (
	"geo/config"
	"geo/run"
	"log"
	"os"
)

func main() {
	//err1 := godotenv.Load(".env")
	//if err1 != nil {
	//	log.Fatalf("Ошибка при загрузке файла .env: %v", err1)
	//}

	conf := config.NewConfig()

	app := run.NewApp(conf)

	if err := app.Bootstrap().Run(); err != nil {
		log.Println("|main_geo| app run error:", err)
		os.Exit(2)
	}
}
