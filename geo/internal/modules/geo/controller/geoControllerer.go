package geoController

import (
	"encoding/json"
	"geo/internal/entities/geoEntity"
	"geo/internal/modules/geo/service"
	"geo/pkg/responder"
	"net/http"
)

type GeoController struct {
	service service.GeoServicer
	respond responder.Responder
}

func NewGeoController(service service.GeoServicer, respond responder.Responder) GeoControllerer {
	return &GeoController{
		service: service,
		respond: respond,
	}
}

func (g *GeoController) SearchAddressHandler(w http.ResponseWriter, r *http.Request) {

	var requestByUser geoEntity.SearchRequest
	err := json.NewDecoder(r.Body).Decode(&requestByUser)
	if err != nil {
		g.respond.ErrorBadRequest(w, err)
	}

	searchResponse, err := g.service.PrepareSearchRequest(requestByUser)
	if err != nil {
		g.respond.ErrorInternal(w, err)
	}

	g.respond.OutputJSON(w, searchResponse)
}

func (g *GeoController) GeocodeAddressHandler(w http.ResponseWriter, r *http.Request) {

	var request geoEntity.GeocodeRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		g.respond.ErrorBadRequest(w, err)
	}

	suggestions, err := g.service.PrepareGeocodeRequest(request)
	if err != nil {
		g.respond.ErrorInternal(w, err)
	}

	addresses := &geoEntity.GeocodeResponse{}
	for _, adr := range suggestions.Suggestions {
		addresses.Addresses = append(addresses.Addresses,
			&geoEntity.Address{
				Lat:    adr.Data["geo_lat"],
				Lon:    adr.Data["geo_lon"],
				Result: adr.Value,
			})
	}

	g.respond.OutputJSON(w, addresses)
}
